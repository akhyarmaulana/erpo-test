package main

import (
	"github.com/gin-gonic/gin"
	"github.com/pangeranweb/app/modules/reverse_string/controllers"
	"github.com/pangeranweb/app/modules/duplicate_array/controllers"
	"github.com/pangeranweb/app/modules/first_bad_version/controllers"
	"github.com/pangeranweb/app/modules/number_one_bit/controllers"
)

func setupRouter() *gin.Engine {
	r := gin.Default()
	r.Use(CORSMiddleware())
	/* Category API */
	
	r.POST("/api/reverse", reverse_string_controller.ReverseString)
	r.POST("/api/duplicate-array", duplicate_array_controller.DuplicateArray)
	r.POST("/api/first-bad-version", first_bad_version_controller.FirstBadVersion)
	r.POST("/api/number-one-bit", number_one_bit_controller.NumberOneBit)
	return r
}

func CORSMiddleware() gin.HandlerFunc {
    return func(c *gin.Context) {
        c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
        c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
        c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
        c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT")

        if c.Request.Method == "OPTIONS" {
            c.AbortWithStatus(204)
            return
        }

        c.Next()
    }
}


func main() {
	r := setupRouter()
	r.Run(":8080")
}
