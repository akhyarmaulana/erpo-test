package reverse_string_controller

import (
	"regexp"
	"github.com/gin-gonic/gin"
	"github.com/pangeranweb/app/utils"
)

type Request struct {
    InputString string `json:"input_string" binding:"required"`
    K int `json:"k" binding:"required"`
}

type Response struct {
	Output string `json:"output"`
}

type ResponseError struct {
	Error string `json:"error"`
}

func Reverse(s string) string {
	r := []rune(s)
	for i, j := 0, len(r)-1; i < len(r)/2; i, j = i+1, j-1 {
		r[i], r[j] = r[j], r[i]
	}
	return string(r)
}

var ReverseString = func(c *gin.Context) {	
	//input
	var r Request
	var error_message string
	c.BindJSON(&r)
	text := r.InputString
	limit := r.K

	//validation
	re := regexp.MustCompile("^[a-z ]*$")
	sts_text := re.MatchString(text)
	if (sts_text == false) {
		error_message = "text input must be lowercase english leter"
	}

	if (limit < 1 || limit > 10000) {
		error_message = "the 'K' value need to be 1 until 10000"
	}

	need_to_reversed := ""
	no_need_to_reversed := ""
	string_len := len(text)
	if (string_len > limit) {
		need_to_reversed = text[0:limit]
		no_need_to_reversed = text[limit:string_len]
	} else {
		need_to_reversed = text[0:string_len]
	}

	output := Reverse(need_to_reversed) + no_need_to_reversed

	//Output response
	if (error_message != "") {
		reponse_error := ResponseError{}
		reponse_error.Error = error_message
		c.JSON(400, utils.Reponse(400, reponse_error))
	} else {
		reponse := Response{}
		reponse.Output = output
		c.JSON(200, utils.Reponse(200, reponse))
	}
}