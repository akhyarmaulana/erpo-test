package number_one_bit_controller

import (
	"math/bits"
	"github.com/gin-gonic/gin"
	"github.com/pangeranweb/app/utils"
)

type Request struct {
    Input uint `json:"input" binding:"required"`
}

type Response struct {
    Output int `json:"output" binding:"required"`
}

var NumberOneBit = func(c *gin.Context) {	
	var r Request
	c.BindJSON(&r)
	input := r.Input
	
	count := 0
    for input != 0 {
        index := bits.TrailingZeros(input)
        input &= ^(1 << uint(index))
        count++
    }

	reponse := Response{}
	reponse.Output = count
	c.JSON(200, utils.Reponse(200, reponse))
}