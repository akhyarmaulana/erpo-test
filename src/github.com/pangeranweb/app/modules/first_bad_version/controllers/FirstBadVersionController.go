package first_bad_version_controller

import (
	"github.com/gin-gonic/gin"
	"github.com/pangeranweb/app/utils"
)

type Request struct {
    HowManyVersion int `json:"how_many_version" binding:"required"`
    WhichIsBadVersion int `json:"which_is_bad_version" binding:"required"`
}

type Response struct {
	HowManyCall int `json:"how_many_call"`
	WhichIsBadVersion int `json:"which_is_bad_version"`
}

var FirstBadVersion = func(c *gin.Context) {	
	var r Request
	var the_bad_version int
	var counter int

	c.BindJSON(&r)
	loop := r.HowManyVersion // represent the looping
	bad := r.WhichIsBadVersion // represent the bad version

	for i := 1; i < loop; i++ {
		if (the_bad_version > 0) { // if we find the bad guy
			break;
		} else {
			counter++
			if (isBadVersion(i , bad)) {
				the_bad_version = i
			}
		}			
	}

	reponse := Response{}
	reponse.HowManyCall = counter
	reponse.WhichIsBadVersion = the_bad_version
	c.JSON(200, utils.Reponse(200, reponse))
}

func isBadVersion(n int, bad int) (bool) {
	if (n == bad) {
		return true
	} else {
		return false
	}
}