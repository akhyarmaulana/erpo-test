package duplicate_array_controller

import (
	"sort"
	"github.com/gin-gonic/gin"
	"github.com/pangeranweb/app/utils"
)

type Request struct {
    Input []int `json:"input" binding:"required"`
}

type Response struct {
	ArrayLength int `json:"array_length"`
	ResultArray []int `json:"result_array"`
}

var DuplicateArray = func(c *gin.Context) {	
	var r Request
	var new_arr []int

	c.BindJSON(&r)
	arr := r.Input
	sort.Ints(arr)

	for i := 0; i < len(arr); i++ {
		if (i > 0) {
			if (arr[(i-1)] == arr[i]) {
				continue;
			}
		}
		new_arr = append(new_arr, arr[i])
	}

	reponse := Response{}
	reponse.ArrayLength = len(new_arr)
	reponse.ResultArray = new_arr
	c.JSON(200, utils.Reponse(200, reponse))
}