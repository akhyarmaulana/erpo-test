package utils

type Response struct {
	Code int `json:"code"`
	Data interface{} `json:"data"`
}

func Reponse(code int, data interface{}) (Response) {

	response := Response{}
	response.Code = code
	response.Data = data

	return response
}