# GO Framework

Simple Go-Lang Project using Gin and Gorm

## Install

Deploy application just need to docker-compose up

`$ docker-compose up`

## Install GO Vendor

Go to docker container SSH

`$ docker-compose exec web sh`

Open folder to /src/github.com/pangeranweb/app then install with govendor.

### Initial install

Go to docker container SSH

`$ docker-compose exec web sh`

Initial install

`$ govendor install +local`

### Install/Add new package, example :

Go to docker container SSH

`$ docker-compose exec web sh`

Add new package

`$ govendor fetch github.com/jinzhu/gorm`
