#!/bin/bash
# add new component here
clear
echo "\nGin & Tonic API Skelleton by Akhyar"
echo "-------------------------------------------"
echo "0. Dependency Install"
go get github.com/gin-gonic/gin
go get github.com/jinzhu/gorm
go get github.com/jinzhu/gorm/dialects/postgres
go get github.com/joho/godotenv
cd /src/github.com/pangeranweb/app && govendor install +local
echo "1. Build app"
go install github.com/pangeranweb/app
echo "2. Run Migration"
# no database no need to migration
# go run src/github.com/pangeranweb/app/migrations/*.go init
# go run src/github.com/pangeranweb/app/migrations/*.go
echo "3. Run app"
/go/bin/app
